require('dotenv').config()
const pgPromise = require('pg-promise');

const pgp = pgPromise({}); // Empty object means no additional config required

const config = {
    // host: process.env.POSTGRES_HOST,
    // port: process.env.POSTGRES_PORT,
    // database: process.env.POSTGRES_DB,
    // user: process.env.POSTGRES_USER,
    // password: process.env.POSTGRES_PASSWORD
    host: 'localhost',
    port: 5432,
    database: 'agrobilbao',
    // database: 'projectdummy',
    user: 'postgres',
    password: 'platzi'
};

const db = pgp(config);

// db.one('select * from project')
//     .then(res => {
//         console.log(res);
//     });


exports.db = db;
