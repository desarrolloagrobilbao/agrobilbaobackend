const graphql = require("graphql");
const db = require("../config/pgAdaptor").db;
const { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLBoolean, GraphQLInt, GraphQLNonNull } = graphql;
const {
  ProjectType,
  TipoDocumentoType,
  LugarType,
  TipoPersonaType,
  PersonaType,
  PedidoType,
  PersonaXPedidoType,
  FacturaType
} = require("../model/types");

const { PersonaInput, PedidoInput, PersonaPedidoInput } = require("../model/inputs");

const RootMutation = new GraphQLObjectType({
  name: "RootMutationType",
  type: "Mutation",
  fields: {
    addTipoDocumento: {
      type: TipoDocumentoType,
      args: {
        iniciales: { type: GraphQLString },
        nombre_tipo_documento: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `INSERT INTO produccion.tipo_documento
        (iniciales, nombre_tipo_documento) 
        VALUES ($1, $2) RETURNING iniciales`;
        const values = [
          args.iniciales,
          args.nombre_tipo_documento
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    updateTipoDocumento: {
      type: TipoDocumentoType,
      args: {
        id_tipo_documento: { type: GraphQLID },
        iniciales: { type: GraphQLString },
        nombre_tipo_documento: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `UPDATE produccion.tipo_documento SET iniciales = $2, nombre_tipo_documento = $3
                        where id_tipo_documento = $1 RETURNING iniciales`;
        const values = [
          args.id_tipo_documento,
          args.iniciales,
          args.nombre_tipo_documento
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    deleteTipoDocumento: {
      type: TipoDocumentoType,
      args: {
        id_tipo_documento: { type: GraphQLID }
      },
      resolve(parentValue, args) {
        const query = `DELETE FROM produccion.tipo_documento where id_tipo_documento = $1 RETURNING id_tipo_documento`;
        const values = [
          args.id_tipo_documento
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    addLugar: {
      type: LugarType,
      args: {
        nombre_lugar: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `INSERT INTO produccion.lugar (nombre_lugar) VALUES ($1) RETURNING nombre_lugar`;
        const values = [
          args.nombre_lugar
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    updateLugar: {
      type: LugarType,
      args: {
        id_lugar: { type: GraphQLID },
        nombre_lugar: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `UPDATE produccion.lugar SET nombre_lugar = $2 where id_lugar = $1 RETURNING nombre_lugar`;
        const values = [
          args.id_lugar,
          args.nombre_lugar
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    deleteLugar: {
      type: LugarType,
      args: {
        id_lugar: { type: GraphQLID }
      },
      resolve(parentValue, args) {
        const query = `DELETE FROM produccion.lugar where id_lugar = $1 RETURNING id_lugar`;
        const values = [
          args.id_lugar
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    addTipoPersona: {
      type: TipoPersonaType,
      args: {
        nombre_tipo_persona: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `INSERT INTO produccion.tipo_persona (nombre_tipo_persona) VALUES ($1) RETURNING nombre_tipo_persona`;
        const values = [
          args.nombre_tipo_persona
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    updateTipoPersona: {
      type: TipoPersonaType,
      args: {
        id_tipo_persona: { type: GraphQLID },
        nombre_tipo_persona: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `UPDATE produccion.tipo_persona SET nombre_tipo_persona = $2 
                      where id_tipo_persona = $1 RETURNING nombre_tipo_persona`;
        const values = [
          args.id_tipo_persona,
          args.nombre_tipo_persona
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    deleteTipoPersona: {
      type: TipoPersonaType,
      args: {
        id_tipo_persona: { type: GraphQLID }
      },
      resolve(parentValue, args) {
        const query = `DELETE FROM produccion.tipo_persona where id_tipo_persona = $1 RETURNING id_tipo_persona`;
        const values = [
          args.id_tipo_persona
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    addPersona: {
      type: PersonaType,
      args: {
        input: {
          type: new GraphQLNonNull(PersonaInput),
        }
      },
      resolve(parentValue, args) {        
        const query = `INSERT INTO produccion.persona 
        (id_tipo_documento,id_tipo_persona,id_lugar,numero_documento,primer_nombre,segundo_nombre,
          primer_apellido,segundo_apellido,email,telefono) values                
          ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) 
          RETURNING id_persona,id_tipo_documento,id_tipo_persona,id_lugar,numero_documento,primer_nombre,segundo_nombre,
          primer_apellido,segundo_apellido,email,telefono`;
        const values = [
          args.input.id_tipo_documento,
          args.input.id_tipo_persona,
          args.input.id_lugar,
          args.input.numero_documento,
          args.input.primer_nombre,
          args.input.segundo_nombre,
          args.input.primer_apellido,
          args.input.segundo_apellido,
          args.input.email,
          args.input.telefono
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    updatePersona: {
      type: PersonaType,
      args: {
        input: {
          type: new GraphQLNonNull(PersonaInput),
        }
      },
      resolve(parentValue, args) {
        const query = `UPDATE produccion.persona SET
                      id_tipo_documento = $2, id_tipo_persona = $3,id_lugar = $4,numero_documento = $5,
                      primer_nombre = $6,segundo_nombre = $7, primer_apellido = $8,segundo_apellido = $9,
                      email = $10,telefono = $11 where id_persona = $1 RETURNING id_persona,id_tipo_documento,id_tipo_persona,id_lugar,numero_documento,primer_nombre,segundo_nombre,
                      primer_apellido,segundo_apellido,email,telefono`;
        const values = [
          args.input.id_persona,
          args.input.id_tipo_documento,
          args.input.id_tipo_persona,
          args.input.id_lugar,
          args.input.numero_documento,
          args.input.primer_nombre,
          args.input.segundo_nombre,
          args.input.primer_apellido,
          args.input.segundo_apellido,
          args.input.email,
          args.input.telefono
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    deletePersona: {
      type: PersonaType,
      args: {
        id_persona: { type: GraphQLID }
      },
      resolve(parentValue, args) {
        const query = `DELETE FROM produccion.persona where id_persona = $1 RETURNING id_persona`;
        const values = [
          args.id_persona
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    addPedido: {
      type: PedidoType,
      args: {
        input: {
          type: new GraphQLNonNull(PedidoInput),
        }
      },
      resolve(parentValue, args) {        
        const query = `INSERT INTO produccion.pedido 
                       (id_persona,cantidad,fecha_pedido,fecha_entrega,direccion,descripcion,tarifa,valor_flete)
                       VALUES ($1,$2,now(),$4,$5,$6,$7,$8) 
                       RETURNING id_pedido, id_persona,cantidad,fecha_pedido,fecha_entrega,direccion,descripcion,tarifa,valor_flete`;
        const values = [
          args.input.id_persona,
          args.input.cantidad,
          args.input.fecha_pedido,
          args.input.fecha_entrega,
          args.input.direccion,
          args.input.descripcion,
          args.input.tarifa,
          args.input.valor_flete
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    updatePedido: {
      type: PedidoType,
      args: {
        input: {
          type: new GraphQLNonNull(PedidoInput),
        }
      },
      resolve(parentValue, args) {
        const query = `UPDATE produccion.pedido SET
                      id_persona = $2,cantidad = $3,fecha_pedido = fecha_pedido,fecha_entrega = $5,direccion = $6,
                      descripcion = $7,tarifa = $8,valor_flete = $9
                      where id_pedido = $1
                      RETURNING id_pedido, id_persona,cantidad,fecha_pedido,fecha_entrega,direccion,descripcion,tarifa,valor_flete`;
        const values = [
          args.input.id_pedido,
          args.input.id_persona,
          args.input.cantidad,
          args.input.fecha_pedido,
          args.input.fecha_entrega,
          args.input.direccion,
          args.input.descripcion,
          args.input.tarifa,
          args.input.valor_flete
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    deletePedido: {
      type: PedidoType,
      args: {
        id_pedido: { type: GraphQLID }
      },
      resolve(parentValue, args) {
        const query = `DELETE FROM produccion.pedido where id_pedido = $1 RETURNING id_pedido`;
        const values = [
          args.id_pedido
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    addPersonaXPedido: {
      type: PersonaXPedidoType,
      args: {
        input: {
          type: new GraphQLNonNull(PersonaPedidoInput),
        }
      },
      resolve(parentValue, args) {
        const query = `INSERT INTO produccion.personaxpedido 
                       (id_persona,id_pedido,cantidad_asociado,fecha_recepcion,descripcion_pp)
                       VALUES ($1,$2,$3,$4,$5) 
                       RETURNING id_personaxpedido,id_persona,id_pedido,cantidad_asociado,fecha_recepcion,descripcion_pp`;
        const values = [
          args.input.id_persona,
          args.input.id_pedido,
          args.input.cantidad_asociado,
          args.input.fecha_recepcion,
          args.input.descripcion_pp
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    updatePersonaXPedido: {
      type: PersonaXPedidoType,
      args: {
        input: { type: new GraphQLNonNull(PersonaPedidoInput) }        
      },
      resolve(parentValue, args) {
        const query = `UPDATE produccion.personaxpedido SET
                       id_persona= $2,id_pedido= $3,cantidad_asociado= $4,fecha_recepcion= $5,descripcion_pp = $6
                       WHERE id_personaxpedido = $1
                       RETURNING id_personaxpedido,id_persona,id_pedido,cantidad_asociado,fecha_recepcion,descripcion_pp`;
        const values = [
          args.input.id_personaxpedido,
          args.input.id_persona,
          args.input.id_pedido,
          args.input.cantidad_asociado,
          args.input.fecha_recepcion,
          args.input.descripcion_pp
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    deletePersonaXPedido: {
      type: PersonaXPedidoType,
      args: {
        id_personaxpedido: { type: GraphQLID }
      },
      resolve(parentValue, args) {
        const query = `DELETE FROM produccion.personaxpedido
                       WHERE id_personaxpedido = $1
                       RETURNING id_personaxpedido,id_persona,id_pedido,cantidad_asociado,fecha_recepcion,descripcion_pp`;
        const values = [
          args.id_personaxpedido
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    addFactura: {
      type: FacturaType,
      args: {
        id_personaxpedido: { type: GraphQLID },
        valor_pedido_persona: { type: GraphQLInt }
      },
      resolve(parentValue, args) {
        const query = `INSERT INTO produccion.factura 
                       (id_personaxpedido, valor_pedido_persona)
                       VALUES ($1,$2) 
                       RETURNING id_factura,id_personaxpedido, valor_pedido_persona`;
        const values = [
          args.id_personaxpedido,
          args.valor_pedido_persona
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    updateFactura: {
      type: FacturaType,
      args: {
        id_factura: { type: GraphQLID },
        id_personaxpedido: { type: GraphQLID },
        valor_pedido_persona: { type: GraphQLInt }
      },
      resolve(parentValue, args) {
        const query = `UPDATE produccion.factura SET
                       id_personaxpedido = $2, valor_pedido_persona = $3
                       WHERE id_factura = $1
                       RETURNING id_factura,id_personaxpedido, valor_pedido_persona`;
        const values = [
          args.id_factura,
          args.id_personaxpedido,
          args.valor_pedido_persona
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    deleteFactura: {
      type: FacturaType,
      args: {
        id_factura: { type: GraphQLID }
      },
      resolve(parentValue, args) {
        const query = `DELETE FROM produccion.factura
                       WHERE id_factura = $1
                       RETURNING id_factura,id_personaxpedido, valor_pedido_persona`;
        const values = [
          args.id_factura
        ];
        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    }

  }
});

exports.mutation = RootMutation;