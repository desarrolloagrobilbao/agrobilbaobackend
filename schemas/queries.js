var jwt = require('jsonwebtoken');
const { db } = require('../config/pgAdaptor');
const { GraphQLObjectType, GraphQLID, GraphQLList, GraphQLString } = require('graphql');
const {
	TipoDocumentoType,
	LugarType,
	TipoPersonaType,
	PersonaType,
	PedidoType,
	UsuarioType,
	PersonaXPedidoType,
	FacturaType,
	UserType,
	TokenType
} = require('../model/types');

const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	type: 'Query',
	fields: {
		tipo_documento: {
			type: TipoDocumentoType,
			args: { id: { type: GraphQLID } },
			resolve(parentValue, args) {
				const query = `SELECT * FROM produccion.tipo_documento where id_tipo_documento = $1`;
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		tipo_documentos: {
			type: new GraphQLList(TipoDocumentoType),
			resolve(parentValue, args) {
				const query = `SELECT * FROM produccion.tipo_documento`;
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		lugar: {
			type: LugarType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				const query = 'select * from produccion.lugar where id_lugar = $1';
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		lugares: {
			type: new GraphQLList(LugarType),
			resolve(parent, args) {
				const query = 'select * from produccion.lugar';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		tipo_persona: {
			type: TipoPersonaType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				const query = 'select * from produccion.tipo_persona where id_tipo_persona = $1';
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		tipo_personas: {
			type: new GraphQLList(TipoPersonaType),
			resolve(parent, args) {
				const query = 'select * from produccion.tipo_persona order by id_persona desc';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		persona: {
			type: PersonaType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				const query = 'select * from produccion.persona where id_persona = $1';
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		personas: {
			type: new GraphQLList(PersonaType),
			resolve(parent, args) {
				const query = 'select * from produccion.persona order by id_persona desc';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		clientes: {
			type: new GraphQLList(PersonaType),
			resolve(parent, args) {
				const query = 'select * from produccion.persona where id_tipo_persona = 2;';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		pedido: {
			type: PedidoType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				// console.log(args.id);

				const query = 'select * from produccion.pedido where id_pedido = $1';
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		pedidos: {
			type: new GraphQLList(PedidoType),
			resolve(parent, args) {
				const query = 'select * from produccion.pedido order by id_pedido desc';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		usuario: {
			type: UsuarioType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				console.log(args.id);

				const query = 'select * from produccion.usuario where id_usuario = $1';
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		usuarios: {
			type: new GraphQLList(UsuarioType),
			resolve(parent, args) {
				const query = 'select * from produccion.usuario';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		personaxpedido: {
			type: PersonaXPedidoType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				console.log(args.id);

				const query = 'select * from produccion.personaxpedido where id_personaxpedido = $1';
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		personasxpedidos: {
			type: new GraphQLList(PersonaXPedidoType),
			resolve(parent, args) {
				const query = 'select * from produccion.personaxpedido order by id_personaxpedido desc';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},
		factura: {
			type: FacturaType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				// console.log(args.id);

				const query = 'select * from produccion.factura where id_factura = $1';
				const values = [ args.id ];
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		facturas: {
			type: new GraphQLList(FacturaType),
			resolve(parent, args) {
				const query = 'select * from produccion.factura order by id_factura desc';
				return db.query(query).then((res) => res).catch((err) => err);
			}
		},	
		user: {
			type: TokenType,
			args: {
				user: { type: GraphQLString},
				password: { type: GraphQLString}
			},
			resolve(parent, args){
				const query = `select * from produccion.usuario u
				join produccion.persona p using(id_persona) where p.numero_documento = $1 and u.clave = md5($2)`;
				const values = [ args.user, args.password ];
				return db.one(query, values).then((res) => {
					// console.log(res);
					let token = jwt.sign(res, 'shhhhh');
					// console.log({'token':token});
					return {'token':token};					
				}).catch((err) => {});
			}
		}
	}
});

exports.query = RootQuery;
