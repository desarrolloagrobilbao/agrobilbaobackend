"use strict";
const graphql = require("graphql");
const express = require("express");
const expressGraphQl = require("express-graphql");
const cors = require("cors");
const { GraphQLSchema } = graphql;
const { query } = require("./schemas/queries");
const { mutation } = require("./schemas/mutations");


const schema = new GraphQLSchema({
  query,
  mutation
});

var app = express();

// Allow cross-origin
app.use(cors())

app.use(
  '/api',
  expressGraphQl({
    schema: schema,
    graphiql: true,
    pretty: true
  })
);

const PORT = process.env.PORT || 4000;

app.listen(PORT, () =>
  console.log(`GraphQL server running on localhost: ${PORT}`)
);