const graphql = require('graphql');
const { GraphQLID, GraphQLString, GraphQLInt, GraphQLInputObjectType } = graphql;

const PersonaInput = new GraphQLInputObjectType({
	name: 'PersonaInput',
	description: 'Input user payload',
	fields: () => ({
		id_persona: { type: GraphQLID },
		id_tipo_documento: { type: GraphQLID },
		id_tipo_persona: { type: GraphQLID },
		id_lugar: { type: GraphQLID },
		numero_documento: { type: GraphQLString },
		primer_nombre: { type: GraphQLString },
		segundo_nombre: { type: GraphQLString },
		primer_apellido: { type: GraphQLString },
		segundo_apellido: { type: GraphQLString },
		email: { type: GraphQLString },
		telefono: { type: GraphQLInt }
	})
});

const PedidoInput = new GraphQLInputObjectType({
	name:'PedidoInput',
	description:'Input order payload',
	fields: () => ({
		id_pedido: { type: GraphQLID },
		id_persona: { type: GraphQLID },
        cantidad: { type: GraphQLInt },
        fecha_pedido: { type: GraphQLString },
        fecha_entrega: { type: GraphQLString },
        direccion: { type: GraphQLString },
        descripcion: { type: GraphQLString },
        tarifa: { type: GraphQLInt },
        valor_flete: { type: GraphQLInt }
	})
});

const PersonaPedidoInput = new GraphQLInputObjectType({
	name: 'PersonaPedidoInput',
	descripcion: 'input user pay load',
	fields: () =>({
		id_personaxpedido: { type: GraphQLID },
		id_persona: { type: GraphQLID },
        id_pedido: { type: GraphQLID },
        cantidad_asociado: { type: GraphQLInt },
        fecha_recepcion: { type: GraphQLString },
        descripcion_pp: { type: GraphQLString }
	})
});

this.PersonaInput = PersonaInput;
this.PedidoInput = PedidoInput;
this.PersonaPedidoInput = PersonaPedidoInput;
