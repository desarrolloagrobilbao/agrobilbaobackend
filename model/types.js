const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLScalarType } = graphql;
const { db } = require('../config/pgAdaptor');

const Date = new GraphQLScalarType({
	name: 'Date',
	serialize(value) {
		return value;
	}
});

const TipoDocumentoType = new GraphQLObjectType({
	name: 'TipoDocumento',
	type: 'Query',
	fields: {
		id_tipo_documento: { type: GraphQLInt },
		iniciales: { type: GraphQLString },
		nombre_tipo_documento: { type: GraphQLString }
	}
});

const LugarType = new GraphQLObjectType({
	name: 'Lugar',
	type: 'Query',
	fields: {
		id_lugar: { type: GraphQLInt },
		nombre_lugar: { type: GraphQLString }
	}
});

const TipoPersonaType = new GraphQLObjectType({
	name: 'TipoPersona',
	type: 'Query',
	fields: {
		id_tipo_persona: { type: GraphQLInt },
		nombre_tipo_persona: { type: GraphQLString }
	}
});

const PersonaType = new GraphQLObjectType({
	name: 'Persona',
	type: 'Query',
	fields: () => ({
		id_persona: { type: GraphQLInt },
		tipo_documento: {
			type: TipoDocumentoType,
			resolve: (data) => {
				// console.log(data);
				const query = `SELECT * FROM produccion.tipo_documento WHERE id_tipo_documento=$1`;
				const values = data.id_tipo_documento;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		lugar: {
			type: LugarType,
			resolve: (data) => {
				const query = `SELECT * FROM produccion.lugar WHERE id_lugar=$1`;
				const values = data.id_tipo_documento;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		tipo_persona: {
			type: TipoPersonaType,
			resolve: (data) => {
				// console.log(data);
				const query = `SELECT * FROM produccion.tipo_persona WHERE id_tipo_persona=$1`;
				const values = data.id_tipo_persona;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		numero_documento: { type: GraphQLString },
		primer_nombre: { type: GraphQLString },
		segundo_nombre: { type: GraphQLString },
		primer_apellido: { type: GraphQLString },
		segundo_apellido: { type: GraphQLString },
		email: { type: GraphQLString },
		telefono: { type: GraphQLInt }
	})
});

const PedidoType = new GraphQLObjectType({
	name: 'Pedido',
	type: 'Query',
	fields: () => ({
		id_pedido: { type: GraphQLInt },
		persona: {
			type: PersonaType,
			resolve: (data) => {
				// console.log(data);
				const query = `SELECT * FROM produccion.persona WHERE id_persona=$1`;
				const values = data.id_persona;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		cantidad: { type: GraphQLInt },
		fecha_pedido: { type: Date },
		fecha_entrega: { type: Date },
		direccion: { type: GraphQLString },
		descripcion: { type: GraphQLString },
		tarifa: { type: GraphQLInt },
		valor_flete: { type: GraphQLInt }
	})
});

const UsuarioType = new GraphQLObjectType({
	name: 'Usuario',
	type: 'Query',
	fields: () => ({
		id_usuario: { type: GraphQLInt },
		persona: {
			type: PersonaType,
			resolve: (data) => {
				// console.log(data);
				const query = `SELECT * FROM produccion.persona WHERE id_persona=$1`;
				const values = data.id_persona;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		clave: { type: GraphQLString }
	})
});

const PersonaXPedidoType = new GraphQLObjectType({
	name: 'PersonaXPedido',
	type: 'Query',
	fields: () => ({
		id_personaxpedido: { type: GraphQLInt },
		persona: {
			type: PersonaType,
			resolve: (data) => {
				// console.log(data);
				const query = `SELECT * FROM produccion.persona WHERE id_persona=$1`;
				const values = data.id_persona;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		pedido: {
			type: PedidoType,
			resolve: (data) => {
				// console.log(data);
				const query = `SELECT * FROM produccion.pedido WHERE id_pedido=$1`;
				const values = data.id_pedido;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		cantidad_asociado: { type: GraphQLInt },
		fecha_recepcion: { type: Date },
		descripcion_pp: { type: GraphQLString }
	})
});

const FacturaType = new GraphQLObjectType({
	name: 'Factura',
	type: 'Query',
	fields: () => ({
		id_factura: { type: GraphQLInt },
		personaxpedido: {
			type: PersonaXPedidoType,
			resolve: (data) => {
				// console.log(data);
				const query = `SELECT * FROM produccion.personaxpedido WHERE id_personaxpedido=$1`;
				const values = data.id_personaxpedido;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		valor_pedido_persona: { type: GraphQLInt },
		descuento: { type: GraphQLInt },
		valor_factura: { type: GraphQLInt }
	})
});

const UserType = new GraphQLObjectType({
	name: 'user',
	type: 'Query',
	fields: {
		id_usuario: { type: GraphQLInt },
		persona: {
			type: PersonaType,
			resolve: (data) => {
				console.log(data);
				const query = `SELECT * FROM produccion.persona WHERE id_persona=$1`;
				const values = data.id_persona;
				return db.one(query, values).then((res) => res).catch((err) => err);
			}
		},
		clave: { type: GraphQLString }
	}
});

const TokenType = new GraphQLObjectType({
	name: 'token',
	type: 'Query',
	fields: {
		token: { type: GraphQLString }
	}
});

exports.TipoDocumentoType = TipoDocumentoType;
exports.LugarType = LugarType;
exports.TipoPersonaType = TipoPersonaType;
exports.PersonaType = PersonaType;
exports.PedidoType = PedidoType;
exports.UsuarioType = UsuarioType;
exports.PersonaXPedidoType = PersonaXPedidoType;
exports.FacturaType = FacturaType;
exports.UserType = UserType;
exports.TokenType = TokenType;
